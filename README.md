# Jai VMA

Jai bindings of [Vulkan® Memory Allocator](https://gpuopen.com/vulkan-memory-allocator/).

Should work on both Windows and Linux. Feel free to submit a pull request if you manage to make it build on MacOS.

# Usage

**Pass this parameter at module import, in order to have the VMA dynamic library automatically copied over with your output. Otherwise, you'll have to copy it over manually in order to use VMA.**

```
#import "VMA"(OUTPUT_DLL_PATH = "C:/your_jai_program/target_dir");
```

# Credit

Kofu on the Jai discord created the first version of these bindings.
